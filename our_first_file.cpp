#include "pch.h"
#include <iostream>
#include <string> 
#include <time.h>
#include <iomanip> 
#include <conio.h>
#include <windows.h>
const int NumberOfLabels = 7, OpeningX = 10, OpeningY = 5;
int ItemOfMenu, MatrixA[13][13], RangeOfNumbers = 32, SizeOfMatrix = 13;
int LeftEdge = -18, RightEdge = RangeOfNumbers + LeftEdge - 1, i, j;
char LabelOfMenu;

HANDLE hConsole, hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
using namespace std;
void OutputMatrix()
void NewMatrix()
void DiagonalUpper()
void ChangeRange()
void DiagonalLower()
void ReplacementByNewNumbers()
void GotoXY()
void SetColor()
void WriteMenuToScreen()
void WriteSelectParagraph()
void WriteParagraphNormal()
void main() {
	srand(time(0));
	for (i = 0; i != SizeOfMatrix; i++) {
		for (j = 0; j != SizeOfMatrix; j++) {
			MatrixA[i][j] =  rand()%(RangeOfNumbers+1) + LeftEdge;	
		}
	}
	system("cls");
	ItemOfMenu = 0;
	WriteMenuToScreen(OpeningX, OpeningY);
	WriteSelectParagraph(OpeningX, OpeningY + ItemOfMenu);
	do {
		LabelOfMenu = _getch();
		if (LabelOfMenu != 13) {
			LabelOfMenu = _getch();
			switch (LabelOfMenu) 
			{
			case 80:
				if (ItemOfMenu < NumberOfLabels-1) {
					WriteParagraphNormal(OpeningX, OpeningY + ItemOfMenu);
					ItemOfMenu++;
					WriteSelectParagraph(OpeningX, OpeningY + ItemOfMenu);
				}
				break;
			case 72:
				if (ItemOfMenu > 0) {
					WriteParagraphNormal(OpeningX, OpeningY + ItemOfMenu);
					ItemOfMenu--;
					WriteSelectParagraph(OpeningX, OpeningY + ItemOfMenu);
				}
				break;
			}
		}
		else {
			if (LabelOfMenu == 13) {
				switch (ItemOfMenu) {
				case 0:
					OutputMatrix();
					break;
				case 1:
					NewMatrix();
					break;
				case 2:
					DiagonalUpper();
					break;
				case 3:
					ChangeRange();
					break;
				case 4:
					DiagonalLower();
					break;
				case 5:
					ReplacementByNewNumbers();
					break;
				case 6:
					LabelOfMenu = 27;
					break;
				}
			}
			WriteMenuToScreen(OpeningX, OpeningY);
			WriteSelectParagraph(OpeningX, OpeningY + ItemOfMenu);
		}
	} while (LabelOfMenu != 27);
}

void NewMatrix() {
	int i1, j1, sizeOfNewMatrix;
	system("cls");
	SetColor(7, 0);
	cout << "Enter the size of the new matrix.\n";
	cin >> sizeOfNewMatrix;
	if (sizeOfNewMatrix > 13) {
		sizeOfNewMatrix = 13;
		cout << "You have entered an invalid number. Try again.\n";
		system("pause");
		NewMatrix();
		return;
	}
	else {
		for (i1 = 0; i1 != sizeOfNewMatrix; i1++) {
			for (j1 = 0; j1 != sizeOfNewMatrix; j1++) {
				MatrixA[i1][j1] = rand() % (RangeOfNumbers + 1) + LeftEdge;
			}
		}
	}
	SizeOfMatrix = sizeOfNewMatrix;
}

void OutputMatrix() {
	int i2, j2;
	system("cls");
	SetColor(7, 0);
	for (i2 = 0; i2 < SizeOfMatrix; i2++)
	{
		for (j2 = 0; j2 < SizeOfMatrix; j2++) {
			cout << setw(4) << MatrixA[i2][j2];
		}
		cout << '\n';
	}
	system("pause");
}

void ChangeRange() {
	system("cls");
	SetColor(7, 0);
	cout << "Enter the left border of the range\n";
	cin >> LeftEdge;
	cout << "Enter the right border of the range\n";
	cin >> RightEdge;
	if (LeftEdge >= RightEdge) {
		cout << "You entered the wrong range. Try again.\n";
		system("pause");
		ChangeRange();
		return;
	}
	else {
		if (LeftEdge > 0) {
			RangeOfNumbers = RightEdge - LeftEdge + 1;
		}
		else if (RightEdge < 0) {
			RangeOfNumbers = abs(LeftEdge) - abs(RightEdge) + 1;
		}
		else {
			RangeOfNumbers = RightEdge + abs(LeftEdge) + 1;
		}
		NewMatrix();
	}
}

void ReplacementByNewNumbers() {
	int i3, j3, numberOfChanged = 0, leftEdgeReplace, rightEdgeReplace, valueToReplace;
	system("cls");
	SetColor(7, 0);
	cout << "Enter the left border of the values to be replaced.\n";
	cin >> leftEdgeReplace;
	cout << "Enter the right border of the values to be replaced.\n";
	cin >> rightEdgeReplace;
	cout << "Enter a replacement value.\n";
	cin >> valueToReplace;
	for (i3 = 0; i3 != SizeOfMatrix; i3++) {
		for (j3 = 0; j3 != SizeOfMatrix; j3++) {
			if ((MatrixA[i3][j3] >= leftEdgeReplace) && (MatrixA[i3][j3] <= rightEdgeReplace)) {
				numberOfChanged = numberOfChanged + 1;
				MatrixA[i3][j3] = valueToReplace;
			}
		}
	}
	if (numberOfChanged == 0) {
		cout << "Required elements not found. The matrix hasn't changed.\n";
	}
	else {
		cout << "Successful!" << numberOfChanged << " elements changed.\n";
	}
}

void DiagonalUpper() {
	int i4, j4, max = -32768;
	for (i4 = 0; i4 != SizeOfMatrix; i4++) {
		for (j4 = 0; j4 != SizeOfMatrix; j4++) {
			if ((MatrixA[i4][j4] > max) && (j4 > i4)) {
				max = MatrixA[i4][j4];
			}
		}
	}
	for (i4 = 0; i4 != SizeOfMatrix; i4++) {
		MatrixA[i4][i4] = max;
	}
}

void DiagonalLower() {
	int i5, j5, halfOfMatrix, maxOfJ, minOfJ, max = -32768;
	for (i5 = 1; i5 <= SizeOfMatrix; i5++) {
		for (j5 = 0; j5 != SizeOfMatrix; j5++) {
			if ((MatrixA[i5][j5] > max) and (j5 > i5)) {
				max = MatrixA[i5][j5];
			}
		}
	}
	halfOfMatrix = SizeOfMatrix / 2;
	if (SizeOfMatrix % 2 == 1) {
		MatrixA[halfOfMatrix + 1][halfOfMatrix] = max;
	}
	if (halfOfMatrix % 2 == 0) {
		halfOfMatrix = halfOfMatrix - 1;
	}
	minOfJ = 0;
	maxOfJ = SizeOfMatrix - 1;
	for (i5 = SizeOfMatrix; i5 >= (SizeOfMatrix - halfOfMatrix); i5--) {
		for (j5 = minOfJ; j5 <= maxOfJ; j5++) {
			MatrixA[i5][j5] = max;
		}
		maxOfJ--;
		minOfJ++;
	}
}
void GotoXY(int goX, int goY) {
	COORD coord = { goX, goY };
	SetConsoleCursorPosition(hStdOut, coord);
}
void SetColor(int text, int background) {
	HANDLE hStdOut = GetStdHandle(STD_OUTPUT_HANDLE);
	SetConsoleTextAttribute(hStdOut, (WORD)((background << 4) | text));
}
void WriteMenuToScreen(int menuX, int menuY) {
	string Menu[NumberOfLabels] = { "Display the current matrix",
		"Create a matrix of size N(less than 13)",
		"Replace the values of the main diagonal with the maximum value of the element above it",
		"Change the range of element values",
		"Replace the elements in the lower triangle of the matrix with the maximum element above the main diagonal",
		"Replace the number in the selected range",
		"Exit"};
	int i0;
	system("cls");
	SetColor(7, 0);
	for (i0 = 0; i0 != NumberOfLabels; i0++) {
		GotoXY(menuX, menuY + i0);
		cout << Menu[i0];
	}
}
void WriteSelectParagraph(int secectX, int secectY) {
	string Menu[NumberOfLabels] = { "Display the current matrix",
		"Create a matrix of size N(less than 13)",
		"Replace the values of the main diagonal with the maximum value of the element above it",
		"Change the range of element values",
		"Replace the elements in the lower triangle of the matrix with the maximum element above the main diagonal",
		"Replace the number in the selected range",
		"Exit" };
	SetColor(2, 0);
	GotoXY(secectX, secectY);
	cout << Menu[ItemOfMenu];
}
void WriteParagraphNormal(int normalX, int normalY) {
	string Menu[NumberOfLabels] = { "Display the current matrix",
		"Create a matrix of size N(less than 13)",
		"Replace the values of the main diagonal with the maximum value of the element above it",
		"Change the range of element values",
		"Replace the elements in the lower triangle of the matrix with the maximum element above the main diagonal",
		"Replace the number in the selected range",
		"Exit" };
	SetColor(7, 0);
	GotoXY(normalX, normalY);
	cout << Menu[ItemOfMenu];
}